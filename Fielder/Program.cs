﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fielder
{
    class Program
    {
        static void Main(string[] args)
        {
            //It does not work

            //Converts public fields to public properties
            //This feature means you can exclude the { get; set; } on your properties and use fields instead.

            //PM> Install-Package Fielder.Fody

            var person = new Person();
            person.GivenNames = "Given Name";
            
            Console.WriteLine(person.FullName);

            Console.ReadKey();
        }
    }
}
