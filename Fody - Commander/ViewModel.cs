﻿using Commander;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Fody___Commander
{
    [ImplementPropertyChanged]
    public class ViewModel
    {
        public string InputText { get; set; }

        [OnCommandCanExecute("SubmitCommand")]
        public bool CanSubmit()
        {
            return !string.IsNullOrWhiteSpace(InputText);
        }

        [OnCommand("SubmitCommand")]
        public void Submit(string message)
        {
            MessageBox.Show(message);
        }
    }
}
