﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fody___NullGuard
{
    class Program
    {
        static void Main(string[] args)
        {
            var sample = new Sample();

            sample.SomeMethod(null);

            Console.ReadKey();
        }
    }
}
