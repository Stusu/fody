﻿using NullGuard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[assembly: NullGuard(ValidationFlags.None)] // Sets no guards at the assembly level

namespace Fody___NullGuard
{

    [NullGuard(ValidationFlags.Properties)] // Sets the default guard for class Sample
    public class Sample
    {
        public void SomeMethod(string arg)
        {
            // throws ArgumentNullException if arg is null.
        }

        public void AnotherMethod([AllowNull] string arg)
        {
            // arg may be null here
        }

        public string MethodWithReturn()
        {
            return "qwerty";
        }

        [return: AllowNull]
        public string MethodAllowsNullReturnValue()
        {
            return null;
        }

        // Null checking works for automatic properties too.
        public string SomeProperty { get; set; }

        // can be applied to a whole property
        [AllowNull]
        public string NullProperty { get; set; }

        // Or just the setter.
        public string NullPropertyOnSet { get;[param: AllowNull] set; }
    }
}
