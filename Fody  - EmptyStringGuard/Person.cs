﻿using EmptyStringGuard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fody____EmptyStringGuard
{
    public class Person
    {
        // can be applied to a whole property
        [AllowEmpty]
        public string GivenNames { get; set; }

        // Empty string checking works for automatic properties too.
        public string FamilyName { get; set; }

        public string FullName
        {
            get
            {
                return string.Format("{0} {1}", GivenNames, FamilyName);
            }
        }

        public void SetFamilyName(string arg)
        {
            // throws ArgumentException if arg is empty string.
            FamilyName = arg;
        }

        public void SetGivenName([AllowEmpty] string arg)
        {
            // arg may be empty string here
            GivenNames = arg;
        }




    }
}
