﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fody____EmptyStringGuard
{
    class Program
    {
        static void Main(string[] args)
        {
            //Adds empty string argument checks to an assembly

            //PM> Install-Package EmptyStringGuard.Fody

            var person = new Person();
            person.GivenNames = "Given Name";
            person.FamilyName = "Family Name";

            Console.WriteLine("Press any key to set Given Name to empty string");
            Console.ReadKey();
            person.SetGivenName("");

            Console.WriteLine("Press any key to set Family Name to empty string");
            Console.ReadKey();
            person.SetFamilyName("");

            Console.ReadKey();
        }
    }
}
