﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fody___ToString
{
    [ToString]
    public class Person
    {
        public string GivenNames { get; set; }
        public string FamilyName { get; set; }

        [IgnoreDuringToString]
        public string FullName
        {
            get
            {
                return string.Format("{0} {1}", GivenNames, FamilyName);
            }
        }
    }
}
