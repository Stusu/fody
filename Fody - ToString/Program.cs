﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fody___ToString
{
    class Program
    {
        static void Main(string[] args)
        {
            //Generates ToString method from public properties for class decorated with a [ToString] Attribute.

            //PM> Install-Package ToString.Fody

            var person = new Person();
            person.GivenNames = "Given Name";
            person.FamilyName = "Family Name";

            Console.WriteLine(person.ToString());

            Console.ReadKey();
        }
    }
}
