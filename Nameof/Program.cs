﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nameof
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Method(null);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            Console.ReadKey();
        }

        public static void Method(String methodArgument)
        {
            if (methodArgument == null)
                throw new ArgumentNullException(nameof(methodArgument), "String must not be null");
            if (methodArgument.Length < 42)
                throw new ArgumentException("String not long enough", nameof(methodArgument)); // Yep, ArgumentException's constructor arguments are in the opposite order of ArgumentNullException's constructor arguments.
        }
    }
}

