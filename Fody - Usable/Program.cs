﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fody___Usable
{
    class Program
    {
        static void Main(string[] args)
        {
            Method();

            Console.ReadKey();
        }

        public static void Method()
        {
            var w = File.CreateText("log.txt");
            w.WriteLine("I'm a lumberjack an' I'm ok.");
        }
    }
}

